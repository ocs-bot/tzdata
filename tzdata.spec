Summary: Timezone data
Name: tzdata
Version: 2024a
Release: 1%{?dist}
License: Public Domain
URL: https://www.iana.org/time-zones
Source0: ftp://ftp.iana.org/tz/releases/tzdata%{version}.tar.gz
Source1: ftp://ftp.iana.org/tz/releases/tzcode%{version}.tar.gz
Patch3000: tzdata-china.diff
Patch3001: 0002-Fix-have-snprintf-error.patch
Patch3002: 0003-continue-to-ship-posixrules.patch

BuildRequires: make, glibc
BuildRequires: gawk, perl-interpreter, glibc-common >= 2.5.90-7
BuildArchitectures: noarch

%description
The Time Zone Database (often called tz or zoneinfo) contains code and data
that represent the history of local time for many representative locations
around the globe. 



%prep
%autosetup -c -a 1

echo "%{name}%{version}" >> VERSION



%build
make tzdata.zi

FILES="africa antarctica asia australasia europe northamerica southamerica
       etcetera backward factory"

mkdir -p zoneinfo/{,posix,right}
zic -y ./yearistype -d zoneinfo -L /dev/null -p America/New_York $FILES
zic -y ./yearistype -d zoneinfo/posix -L /dev/null $FILES
zic -y ./yearistype -d zoneinfo/right -L leapseconds $FILES



%install
rm -fr %{buildroot}
install -d %{buildroot}%{_datadir}
cp -prd zoneinfo %{buildroot}%{_datadir}
install -p -m 644 zone.tab zone1970.tab iso3166.tab leap-seconds.list leapseconds tzdata.zi %{buildroot}%{_datadir}/zoneinfo



%files
%license LICENSE
%doc README
%doc theory.html
%doc tz-link.html
%doc tz-art.html
%{_datadir}/zoneinfo



%changelog
* Wed May 29 2024 Upgrade Robot <upbot@opencloudos.tech> - 2024a-1
- Upgrade to version 2024a

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2023c-2
- Rebuilt for OpenCloudOS Stream 23.09

* Thu Jul 13 2023 Wang Guodong <gordonwwang@tencent.com> - 2023c-1
- Upgrade to 2023c

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2022g-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2022g-2
- Rebuilt for OpenCloudOS Stream 23

* Wed May 18 2022 Zhao Zhen <jeremiazhao@tencent.com> - 2022g-1
- updated version to 2022g
- added beijing time

* Wed May 18 2022 Zhao Zhen <jeremiazhao@tencent.com> - 2022a-1
- Initial, remove tzdata-java, timezone files of java will moved into java package.
